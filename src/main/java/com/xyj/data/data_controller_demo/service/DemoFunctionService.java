package com.xyj.data.data_controller_demo.service;

import com.xyj.data.data_controller_demo.domain.DemoFunction;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Lenovo
* @description 针对表【demo_function】的数据库操作Service
* @createDate 2023-12-20 16:37:07
*/
public interface DemoFunctionService extends IService<DemoFunction> {

}
