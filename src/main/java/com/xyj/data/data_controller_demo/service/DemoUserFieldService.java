package com.xyj.data.data_controller_demo.service;

import com.xyj.data.data_controller_demo.domain.DemoUserField;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Lenovo
* @description 针对表【demo_user_field】的数据库操作Service
* @createDate 2023-12-21 11:39:34
*/
public interface DemoUserFieldService extends IService<DemoUserField> {

}
