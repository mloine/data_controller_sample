package com.xyj.data.data_controller_demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xyj.data.data_controller_demo.domain.DemoUserField;
import com.xyj.data.data_controller_demo.service.DemoUserFieldService;
import com.xyj.data.data_controller_demo.mapper.DemoUserFieldMapper;
import org.springframework.stereotype.Service;

/**
* @author Lenovo
* @description 针对表【demo_user_field】的数据库操作Service实现
* @createDate 2023-12-21 11:39:34
*/
@Service
public class DemoUserFieldServiceImpl extends ServiceImpl<DemoUserFieldMapper, DemoUserField>
    implements DemoUserFieldService{

}




