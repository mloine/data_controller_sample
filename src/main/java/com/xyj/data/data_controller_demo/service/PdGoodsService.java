package com.xyj.data.data_controller_demo.service;

import com.xyj.data.data_controller_demo.domain.PdGoods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Lenovo
* @description 针对表【pd_goods】的数据库操作Service
* @createDate 2023-12-20 16:37:07
*/
public interface PdGoodsService extends IService<PdGoods> {

}
