package com.xyj.data.data_controller_demo.service;

import com.xyj.data.data_controller_demo.domain.DemoUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Lenovo
* @description 针对表【demo_user】的数据库操作Service
* @createDate 2023-12-20 10:51:52
*/
public interface DemoUserService extends IService<DemoUser> {

}
