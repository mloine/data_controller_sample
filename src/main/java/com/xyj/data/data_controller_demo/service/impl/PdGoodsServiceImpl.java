package com.xyj.data.data_controller_demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xyj.data.data_controller_demo.domain.PdGoods;
import com.xyj.data.data_controller_demo.service.PdGoodsService;
import com.xyj.data.data_controller_demo.mapper.PdGoodsMapper;
import org.springframework.stereotype.Service;

/**
* @author Lenovo
* @description 针对表【pd_goods】的数据库操作Service实现
* @createDate 2023-12-20 16:37:07
*/
@Service
public class PdGoodsServiceImpl extends ServiceImpl<PdGoodsMapper, PdGoods>
    implements PdGoodsService{

}




