package com.xyj.data.data_controller_demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xyj.data.data_controller_demo.domain.DemoUser;
import com.xyj.data.data_controller_demo.service.DemoUserService;
import com.xyj.data.data_controller_demo.mapper.DemoUserMapper;
import org.springframework.stereotype.Service;

/**
* @author Lenovo
* @description 针对表【demo_user】的数据库操作Service实现
* @createDate 2023-12-20 10:51:52
*/
@Service
public class DemoUserServiceImpl extends ServiceImpl<DemoUserMapper, DemoUser>
    implements DemoUserService{

}




