package com.xyj.data.data_controller_demo.mapper;

import com.xyj.data.data_controller_demo.domain.DemoFunction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Lenovo
* @description 针对表【demo_function】的数据库操作Mapper
* @createDate 2023-12-20 16:37:07
* @Entity com.xyj.data.data_controller_demo.domain.DemoFunction
*/
public interface DemoFunctionMapper extends BaseMapper<DemoFunction> {

}




