package com.xyj.data.data_controller_demo.mapper;

import com.xyj.data.data_controller_demo.domain.DemoUserField;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Lenovo
* @description 针对表【demo_user_field】的数据库操作Mapper
* @createDate 2023-12-21 11:39:34
* @Entity com.xyj.data.data_controller_demo.domain.DemoUserField
*/
public interface DemoUserFieldMapper extends BaseMapper<DemoUserField> {

}




