package com.xyj.data.data_controller_demo.mapper;

import com.xyj.data.data_controller_demo.domain.DemoUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Lenovo
* @description 针对表【demo_user】的数据库操作Mapper
* @createDate 2023-12-20 10:51:52
* @Entity com.xyj.data.data_controller_demo.domain.DemoUser
*/
public interface DemoUserMapper extends BaseMapper<DemoUser> {

}




