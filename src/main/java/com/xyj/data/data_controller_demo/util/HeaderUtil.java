package com.xyj.data.data_controller_demo.util;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xyj.data.data_controller_demo.domain.DemoUser;
import com.xyj.data.data_controller_demo.service.DemoUserService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author xyj
 * @date 2023/12/20
 */
@Component
public class HeaderUtil {
    @Autowired
    DemoUserService demoUserService;

    public static final String AUTHORIZATION_HEADER = "Authorization";

    public DemoUser getUserFromHeader(HttpServletRequest request) {
        String token = request.getHeader(AUTHORIZATION_HEADER);
        if (token == null) {
            return null;
        }
        return demoUserService.getOne(new LambdaQueryWrapper<DemoUser>().eq(DemoUser::getToken, token));
    }
}
