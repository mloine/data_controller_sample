package com.xyj.data.data_controller_demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xyj.data.data_controller_demo.dataFilter.modle.ResultInfo;
import com.xyj.data.data_controller_demo.domain.DemoFiled;
import com.xyj.data.data_controller_demo.domain.DemoFunction;
import com.xyj.data.data_controller_demo.domain.DemoUser;
import com.xyj.data.data_controller_demo.domain.DemoUserField;
import com.xyj.data.data_controller_demo.request.UserFieldRequest;
import com.xyj.data.data_controller_demo.service.DemoFiledService;
import com.xyj.data.data_controller_demo.service.DemoFunctionService;
import com.xyj.data.data_controller_demo.service.DemoUserFieldService;
import com.xyj.data.data_controller_demo.util.HeaderUtil;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author xyj
 * @date 2023/12/20
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/data")
public class DataController {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HeaderUtil headerUtil;

    @Autowired
    private DemoFunctionService demoFunctionService;

    @Autowired
    private DemoFiledService demoFiledService;

    @Autowired
    private DemoUserFieldService demoUserFieldService;

    @GetMapping("/getNowUserTree")
    public ResultInfo getNowUserTree() {
        DemoUser userFromHeader = headerUtil.getUserFromHeader(request);


        List<DemoFunction> list = demoFunctionService.list();
        List<DemoFiled> filedList = demoFiledService.list();

        if (userFromHeader != null) {
            List<DemoUserField> ufs = demoUserFieldService.list(new LambdaQueryWrapper<DemoUserField>().eq(DemoUserField::getUserId, userFromHeader.getId()));

            HashMap<Long, DemoFiled> fieldMap = new HashMap<>();
            for (DemoFiled demoFiled : filedList) {
                fieldMap.put(demoFiled.getId(), demoFiled);
            }
            for (DemoUserField uf : ufs) {
                DemoFiled demoFiled = fieldMap.get(uf.getFieldId());
                if (demoFiled != null) {
                    demoFiled.setConditionTxt(uf.getConditionTxt());
                }
            }
        }


        HashMap<Long, DemoFunction> keys = new HashMap<>();
        for (DemoFunction demoFunction : list) {
            keys.put(demoFunction.getId(), demoFunction);
        }
        for (DemoFiled demoFiled : filedList) {
            Long functionId = demoFiled.getFunctionId();
            DemoFunction demoFunction = keys.get(functionId);
            if (CollectionUtils.isEmpty(demoFunction.getChildren())) {
                demoFunction.setChildren(new ArrayList<DemoFiled>());
            }
            demoFunction.getChildren().add(demoFiled);
        }


        return ResultInfo.success(keys.size() == 0 ? new ArrayList<>() : keys.values());
    }

    @Transactional
    @PostMapping("/saveOrUpdate")
    public ResultInfo saveOrUpdate(@RequestBody UserFieldRequest req) {
        List<DemoUserField> list = req.getList();
        DemoUser userFromHeader = headerUtil.getUserFromHeader(request);
        if (userFromHeader == null) {
            return ResultInfo.error("用户未登入");
        }
        if (CollectionUtils.isEmpty(list)) {
            return ResultInfo.error("数据为空");
        }
        Long functionId = list.get(0).getFunctionId();


        demoUserFieldService.remove(new LambdaQueryWrapper<DemoUserField>().eq(DemoUserField::getFunctionId, functionId)
                .eq(DemoUserField::getUserId, userFromHeader.getId()));

        for (DemoUserField demoUserField : list) {
            demoUserField.setFieldId(demoUserField.getId());
            demoUserField.setId(null);
            demoUserField.setUserId(userFromHeader.getId());
        }
        demoUserFieldService.saveBatch(list);
        return ResultInfo.success("保存成功");

    }
}
