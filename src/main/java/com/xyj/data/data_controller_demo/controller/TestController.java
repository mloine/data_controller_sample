package com.xyj.data.data_controller_demo.controller;

import com.xyj.data.data_controller_demo.dataFilter.annotation.DataAllow;
import com.xyj.data.data_controller_demo.dataFilter.modle.ResultInfo;
import com.xyj.data.data_controller_demo.dataFilter.util.PageDataFilter;
import com.xyj.data.data_controller_demo.domain.PdGoods;
import com.xyj.data.data_controller_demo.service.PdGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xyj
 * @date 2023/12/20
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/test")
public class TestController {
    @Autowired
    PdGoodsService goodsService;

    @DataAllow(extractor = PageDataFilter.class, value = PdGoods.class, desc = "商品列表")

    @GetMapping("/good")
    public ResultInfo good() {
        return ResultInfo.success(goodsService.list());
    }

    @DataAllow(extractor = PageDataFilter.class, value = PdGoods.class, desc = "商品列表2")
    @GetMapping("/good2")
    public ResultInfo good1() {
        return ResultInfo.success(goodsService.list());
    }
}
