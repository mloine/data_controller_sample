package com.xyj.data.data_controller_demo.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.xyj.data.data_controller_demo.dataFilter.annotation.Allow;
import lombok.Data;

/**
 * @TableName pd_goods
 */
@TableName(value = "pd_goods")
@Data
public class PdGoods implements Serializable {
    /**
     * id
     */
    @TableId
    private Long id;

    /**
     * 商品sku
     */
    @Allow("商品sku")
    private String sku;

    /**
     * 服务时间(天)
     */
    private BigDecimal serverTime;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品描述
     */
    private String goodDesc;

    /**
     * 原价
     */
    @Allow("原价")
    private BigDecimal rawPrice;

    /**
     * 优惠价格
     */
    private BigDecimal discount;

    /**
     * 到手价格
     */
    private BigDecimal price;

    /**
     * 供应商主键
     */
    private Long vendorId;

    /**
     * 供应商名称
     */
    @Allow("供应商名称")
    private String vendorName;

    /**
     * 商品小图片
     */
    private String icorn;

    /**
     * 图片路径
     */
    private String imgUrl;

    /**
     * 服务类型
     */
    @Allow("服务类型")
    private Integer goodsType;

    /**
     * 是否启用
     */
    @Allow("启用")
    private Integer isEnable;

    /**
     * 是否删除
     */
    private Integer deleteFlag;

    /**
     * 创建人ID
     */
    private Long createBy;

    /**
     * 创建人名称
     */
    private String createByName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后更新人ID
     */
    private Long updateBy;

    /**
     * 最后修改人名称
     */
    private String updateByName;

    /**
     * 更新时间
     */
    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PdGoods other = (PdGoods) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getSku() == null ? other.getSku() == null : this.getSku().equals(other.getSku()))
                && (this.getServerTime() == null ? other.getServerTime() == null : this.getServerTime().equals(other.getServerTime()))
                && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
                && (this.getGoodDesc() == null ? other.getGoodDesc() == null : this.getGoodDesc().equals(other.getGoodDesc()))
                && (this.getRawPrice() == null ? other.getRawPrice() == null : this.getRawPrice().equals(other.getRawPrice()))
                && (this.getDiscount() == null ? other.getDiscount() == null : this.getDiscount().equals(other.getDiscount()))
                && (this.getPrice() == null ? other.getPrice() == null : this.getPrice().equals(other.getPrice()))
                && (this.getVendorId() == null ? other.getVendorId() == null : this.getVendorId().equals(other.getVendorId()))
                && (this.getVendorName() == null ? other.getVendorName() == null : this.getVendorName().equals(other.getVendorName()))
                && (this.getIcorn() == null ? other.getIcorn() == null : this.getIcorn().equals(other.getIcorn()))
                && (this.getImgUrl() == null ? other.getImgUrl() == null : this.getImgUrl().equals(other.getImgUrl()))
                && (this.getGoodsType() == null ? other.getGoodsType() == null : this.getGoodsType().equals(other.getGoodsType()))
                && (this.getIsEnable() == null ? other.getIsEnable() == null : this.getIsEnable().equals(other.getIsEnable()))
                && (this.getDeleteFlag() == null ? other.getDeleteFlag() == null : this.getDeleteFlag().equals(other.getDeleteFlag()))
                && (this.getCreateBy() == null ? other.getCreateBy() == null : this.getCreateBy().equals(other.getCreateBy()))
                && (this.getCreateByName() == null ? other.getCreateByName() == null : this.getCreateByName().equals(other.getCreateByName()))
                && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
                && (this.getUpdateBy() == null ? other.getUpdateBy() == null : this.getUpdateBy().equals(other.getUpdateBy()))
                && (this.getUpdateByName() == null ? other.getUpdateByName() == null : this.getUpdateByName().equals(other.getUpdateByName()))
                && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getSku() == null) ? 0 : getSku().hashCode());
        result = prime * result + ((getServerTime() == null) ? 0 : getServerTime().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getGoodDesc() == null) ? 0 : getGoodDesc().hashCode());
        result = prime * result + ((getRawPrice() == null) ? 0 : getRawPrice().hashCode());
        result = prime * result + ((getDiscount() == null) ? 0 : getDiscount().hashCode());
        result = prime * result + ((getPrice() == null) ? 0 : getPrice().hashCode());
        result = prime * result + ((getVendorId() == null) ? 0 : getVendorId().hashCode());
        result = prime * result + ((getVendorName() == null) ? 0 : getVendorName().hashCode());
        result = prime * result + ((getIcorn() == null) ? 0 : getIcorn().hashCode());
        result = prime * result + ((getImgUrl() == null) ? 0 : getImgUrl().hashCode());
        result = prime * result + ((getGoodsType() == null) ? 0 : getGoodsType().hashCode());
        result = prime * result + ((getIsEnable() == null) ? 0 : getIsEnable().hashCode());
        result = prime * result + ((getDeleteFlag() == null) ? 0 : getDeleteFlag().hashCode());
        result = prime * result + ((getCreateBy() == null) ? 0 : getCreateBy().hashCode());
        result = prime * result + ((getCreateByName() == null) ? 0 : getCreateByName().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateBy() == null) ? 0 : getUpdateBy().hashCode());
        result = prime * result + ((getUpdateByName() == null) ? 0 : getUpdateByName().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", sku=").append(sku);
        sb.append(", serverTime=").append(serverTime);
        sb.append(", name=").append(name);
        sb.append(", goodDesc=").append(goodDesc);
        sb.append(", rawPrice=").append(rawPrice);
        sb.append(", discount=").append(discount);
        sb.append(", price=").append(price);
        sb.append(", vendorId=").append(vendorId);
        sb.append(", vendorName=").append(vendorName);
        sb.append(", icorn=").append(icorn);
        sb.append(", imgUrl=").append(imgUrl);
        sb.append(", goodsType=").append(goodsType);
        sb.append(", isEnable=").append(isEnable);
        sb.append(", deleteFlag=").append(deleteFlag);
        sb.append(", createBy=").append(createBy);
        sb.append(", createByName=").append(createByName);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateBy=").append(updateBy);
        sb.append(", updateByName=").append(updateByName);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}