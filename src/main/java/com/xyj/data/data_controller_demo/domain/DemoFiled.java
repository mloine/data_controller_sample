package com.xyj.data.data_controller_demo.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import lombok.Data;

/**
 * @TableName demo_filed
 */
@TableName(value = "demo_filed")
@Data
public class DemoFiled implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     *
     */
    private String name;

    /**
     *
     */
    private String type;

    private String desct;

    /**
     *
     */
    private Long functionId;

    /**
     *
     */
    private Integer deleteFlag;

    @TableField(exist = false)
    private String conditionTxt;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;


}