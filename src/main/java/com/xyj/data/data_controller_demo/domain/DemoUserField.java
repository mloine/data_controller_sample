package com.xyj.data.data_controller_demo.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import lombok.Data;

/**
 * @TableName demo_user_field
 */
@TableName(value = "demo_user_field")
@Data
public class DemoUserField implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     *
     */
    private String name;

    /**
     *
     */
    private String type;

    /**
     *
     */
    private Long functionId;

    /**
     *
     */
    private Integer deleteFlag;

    /**
     *
     */
    private String desct;

    /**
     *
     */
    private Long userId;

    /**
     *
     */
    private String conditionTxt;

    private Long fieldId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

}