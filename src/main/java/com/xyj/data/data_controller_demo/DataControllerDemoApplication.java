package com.xyj.data.data_controller_demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@EnableAspectJAutoProxy
@SpringBootApplication
@MapperScan("com.xyj.data.data_controller_demo.mapper")
public class DataControllerDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataControllerDemoApplication.class, args);
    }

}
