package com.xyj.data.data_controller_demo.request;

import com.xyj.data.data_controller_demo.domain.DemoUserField;
import lombok.Data;

import java.util.List;

/**
 * @author xyj
 * @date 2023/12/21
 */
@Data
public class UserFieldRequest {

    private List<DemoUserField> list;
}
