package com.xyj.data.data_controller_demo.request;

import lombok.Data;

/**
 * @author xyj
 * @date 2023/12/20
 */
@Data
public class LoginRequest {

    private String username;

    private String password;
}
