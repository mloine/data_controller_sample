package com.xyj.data.data_controller_demo.dataFilter.modle;

import lombok.Data;

/**
 * @author xyj
 * @date 2023/12/21
 */
@Data
public class TreeFunctionNode {

    private String type;

    private String desc;


}
