package com.xyj.data.data_controller_demo.dataFilter.core;

/**
 * @author xyj
 * @date 2023/12/13
 */
public interface Extract<T> {

    Object extract(T data);
}
