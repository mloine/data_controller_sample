package com.xyj.data.data_controller_demo.dataFilter.util;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.xyj.data.data_controller_demo.dataFilter.core.Extract;
import com.xyj.data.data_controller_demo.dataFilter.modle.ResultInfo;
import org.springframework.stereotype.Component;

/**
 * @author xyj
 * @date 2023/12/14
 */
@Component
public class PageDataFilter implements Extract<ResultInfo> {


    @Override
    public Object extract(ResultInfo data) {
        Object result = data.getResult();
        if (result == null) {
            return null;
        }


        if (result instanceof Page<?>) {
            return ((Page<?>) result).getRecords();
        }
        return result;
    }
}

