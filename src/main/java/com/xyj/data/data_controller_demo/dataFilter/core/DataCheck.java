package com.xyj.data.data_controller_demo.dataFilter.core;

import java.lang.reflect.Method;

/**
 * @author xyj
 * @date 2023/12/12
 */
public interface DataCheck {

    Object checkAndFilter(Object data, Method method, String url);
}
