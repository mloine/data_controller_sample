package com.xyj.data.data_controller_demo.dataFilter.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author xyj
 * @date 2023/12/14
 */

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Allow {

    /**
     * 描述
     */
    String value();

}
