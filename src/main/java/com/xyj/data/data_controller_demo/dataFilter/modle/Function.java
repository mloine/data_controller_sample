package com.xyj.data.data_controller_demo.dataFilter.modle;

import lombok.Data;

/**
 * @author xyj
 * @date 2023/12/14
 */
@Data
public class Function{

    private Long id;

    private String url;
}
