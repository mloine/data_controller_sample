package com.xyj.data.data_controller_demo.dataFilter.modle;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author xyj
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultInfo {
    private Integer code;
    private String msg;
    private Object result = "null";


    public static ResultInfo success() {
        return new ResultInfo(200, "success", null);
    }

    public static ResultInfo success(Object result ) {
        return new ResultInfo(200, "success", result);
    }


    public static ResultInfo success(int code, String msg) {
        return new ResultInfo(code, msg, null);
    }

    public static ResultInfo success(int code, String msg, Object result) {
        return new ResultInfo(code, msg, result);
    }

    public static ResultInfo error(String msg) {
        return new ResultInfo(500, msg, null);
    }

    public static ResultInfo error(Integer code, String msg) {
        return new ResultInfo(code, msg, null);
    }

    /**
     * 模糊回复, 避免外人查看情况. 不能直接告诉外界出现什么错误
     */
    public static ResultInfo error() {
        return new ResultInfo(500, "system error", null);
    }

}