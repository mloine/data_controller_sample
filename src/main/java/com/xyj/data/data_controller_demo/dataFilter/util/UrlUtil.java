package com.xyj.data.data_controller_demo.dataFilter.util;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.Method;

/**
 * @author xyj
 * @date 2023/12/13
 */
@Component
public class UrlUtil {
    /**
     * 根据给定的方法获取方法对应的URL
     *
     * @param method 方法对象
     * @return 方法对应的URL，如果不存在对应的URL则返回null
     */
    public String getMethodRequestUrl(Method method) {
        RequestMapping annotation = method.getAnnotation(RequestMapping.class);
        PostMapping annotation1 = method.getAnnotation(PostMapping.class);
        GetMapping annotation2 = method.getAnnotation(GetMapping.class);

        if (annotation != null) { // 如果方法上存在RequestMapping注解
            return annotation.value()[0]; // 返回注解中的URL值
        } else if (annotation1 != null) { // 如果方法上存在PostMapping注解
            return annotation1.value()[0]; // 返回注解中的URL值
        } else if (annotation2 != null) { // 如果方法上存在GetMapping注解
            return annotation2.value()[0]; // 返回注解中的URL值
        } else {
            return null; // 如果方法上不存在RequestMapping、PostMapping和GetMapping注解，则返回null
        }
    }

    /**
     * 获取类的URL
     *
     * @param aclass 类对象
     * @return 类上的RequestMapping注解对应的URL值，若不存在则返回null
     */
    public String getClassRequestUrl(Class<?> aclass) {
        RequestMapping annotation = aclass.getAnnotation(RequestMapping.class);
        if (annotation != null) { // 如果类上存在RequestMapping注解
            return annotation.value()[0]; // 返回注解中的URL值
        } else {
            return null; // 如果类上不存在RequestMapping注解，则返回null
        }
    }

    /**
     * 根据指定规则排除代理字符串
     *
     * @param str 要排除的代理字符串
     * @return 排除后的字符串
     */
    public String excludeProxyStr(String str) {
        if (str == null || str.length() == 0) { // 如果字符串为空或长度为0，直接返回原字符串
            return str;
        }
        int i = str.indexOf("$"); // 在字符串中查找字符"$"的位置
        if (i != -1) { // 如果找到了"$"
            return str.substring(0, str.indexOf("$")); // 返回"$"前面的子字符串
        } else {
            return str; // 否则返回原字符串
        }
    }


    /**
     * 根据给定的方法获取完整的URL路径
     *
     * @param method 方法对象
     * @return 完整的URL路径
     */
    public String getCompleteUrlByMethod(Method method) {
        // 获取方法所属的类
        Class<?> declaringClass = method.getDeclaringClass();
        // 获取类的请求URL
        String classUrl = getClassRequestUrl(declaringClass);
        // 获取方法的请求URL
        String methodUrl = getMethodRequestUrl(method);
        // 返回方法的请求URL拼接类的请求URL
        return classUrl + methodUrl;
    }


}
