package com.xyj.data.data_controller_demo.dataFilter.core;

import com.xyj.data.data_controller_demo.dataFilter.annotation.DataAllow;
import com.xyj.data.data_controller_demo.dataFilter.util.FunctionUtil;
import com.xyj.data.data_controller_demo.domain.DemoUserField;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author xyj
 * @date 2023/12/12
 */
@Component
@Slf4j
public class SimpleDataFilter implements DataCheck, ApplicationContextAware {

    @Autowired
    private ConditionManager conditionManager;

    @Autowired
    private FunctionUtil functionUtil;

    private ApplicationContext applicationContext;


    @Override
    public Object checkAndFilter(Object data, Method method, String url) {

        List<DemoUserField> fields = conditionManager.getUserConditionByFunctionId(url);
        if (fields == null) {
            return data;
        }

        DataAllow annotation = method.getAnnotation(DataAllow.class);


        Extract bean = applicationContext.getBean(annotation.extractor());

        Object dataList = bean.extract(data);
        if (dataList == null) {
            return data;
        }
        log.info("功能key：{},过滤前获得数据:{}", url, dataList);

        if (dataList instanceof Collection<?> && ((Collection<?>) dataList).size() > 0) {
            Iterator<?> iterator = ((List<?>) dataList).iterator();
            while (iterator.hasNext()) {
                AtomicBoolean flag = new AtomicBoolean(true);

                Object e = iterator.next();
                for (DemoUserField k : fields) {//字段名字
                    String filedName = k.getName();

                    String conditionTxt = k.getConditionTxt();
                    if (!StringUtils.isEmpty(conditionTxt)) {
                        //有el表达式才做校验
                        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
                        stringObjectHashMap.put(FunctionUtil.PLACEHOLDER_STR, functionUtil.getFieldValueByName(filedName, e));
                        Boolean aBoolean = functionUtil.checkFiled(conditionTxt, stringObjectHashMap);
                        if (!aBoolean) {
                            flag.set(false);
                            break;
                        }
                    }
                }

                if (!flag.get()) {
                    iterator.remove();
                }
            }
        }


        return data;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
