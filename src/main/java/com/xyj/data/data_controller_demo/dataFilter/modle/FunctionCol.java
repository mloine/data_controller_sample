package com.xyj.data.data_controller_demo.dataFilter.modle;

import lombok.Data;

/**
 * @author xyj
 * @date 2023/12/14
 * <p>
 * 返回实体的属性列
 */
@Data
public class FunctionCol {

    private Long id;

    private Long functionId;

    private String colName;

    private String colNameDesc;

    private String colType;
}
