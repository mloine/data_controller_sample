package com.xyj.data.data_controller_demo.dataFilter.annotation;


import com.xyj.data.data_controller_demo.dataFilter.core.Extract;
import com.xyj.data.data_controller_demo.dataFilter.core.NoOperateExtract;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author xyj
 * @date 2023/12/12
 * <p>
 * 标记接口的返回数据需要过滤处理
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DataAllow {

    /**
     * 自定义数据提取器
     * 默认不处理
     *
     * @return
     */
    Class<? extends Extract> extractor() default NoOperateExtract.class;

    /**
     * 需解析的实体类
     */
    Class<?> value();

    /**
     * 描述
     */
    String desc();
}
