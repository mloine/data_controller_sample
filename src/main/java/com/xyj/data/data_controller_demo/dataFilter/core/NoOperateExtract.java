package com.xyj.data.data_controller_demo.dataFilter.core;

import org.springframework.stereotype.Component;

/**
 * @author xyj
 * @date 2023/12/13
 * <p>
 * 不做任何处理的结果提取器
 */
@Component
public class NoOperateExtract implements Extract {


    @Override
    public Object extract(Object data) {
        return data;
    }
}
