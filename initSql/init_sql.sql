/*
 Navicat Premium Data Transfer

 Source Server         : remotePigx
 Source Server Type    : MySQL
 Source Server Version : 80027 (8.0.27)
 Source Host           : 192.168.5.109:3306
 Source Schema         : demo

 Target Server Type    : MySQL
 Target Server Version : 80027 (8.0.27)
 File Encoding         : 65001

 Date: 22/12/2023 09:44:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for demo_filed
-- ----------------------------
DROP TABLE IF EXISTS `demo_filed`;
CREATE TABLE `demo_filed`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci NULL DEFAULT NULL,
  `function_id` bigint NOT NULL,
  `delete_flag` int NOT NULL DEFAULT 0,
  `desct` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_croatian_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of demo_filed
-- ----------------------------
INSERT INTO `demo_filed` VALUES (1, 'rawPrice', 'java.math.BigDecimal', 1, 0, '原价');
INSERT INTO `demo_filed` VALUES (2, 'goodsType', 'java.lang.Integer', 1, 0, '服务类型');
INSERT INTO `demo_filed` VALUES (3, 'vendorName', 'java.lang.String', 1, 0, '供应商名称');
INSERT INTO `demo_filed` VALUES (4, 'sku', 'java.lang.String', 1, 0, '商品sku');
INSERT INTO `demo_filed` VALUES (5, 'isEnable', 'java.lang.Integer', 1, 0, '启用');
INSERT INTO `demo_filed` VALUES (6, 'sku', 'java.lang.String', 2, 0, '商品sku');
INSERT INTO `demo_filed` VALUES (7, 'rawPrice', 'java.math.BigDecimal', 2, 0, '原价');
INSERT INTO `demo_filed` VALUES (8, 'vendorName', 'java.lang.String', 2, 0, '供应商名称');
INSERT INTO `demo_filed` VALUES (9, 'goodsType', 'java.lang.Integer', 2, 0, '服务类型');
INSERT INTO `demo_filed` VALUES (10, 'isEnable', 'java.lang.Integer', 2, 0, '启用');

-- ----------------------------
-- Table structure for demo_function
-- ----------------------------
DROP TABLE IF EXISTS `demo_function`;
CREATE TABLE `demo_function`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci NULL DEFAULT NULL,
  `delete_flag` int NOT NULL DEFAULT 0,
  `desct` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_croatian_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of demo_function
-- ----------------------------
INSERT INTO `demo_function` VALUES (1, '/test/good', 0, '商品列表');
INSERT INTO `demo_function` VALUES (2, '/test/good2', 0, '商品列表2');

-- ----------------------------
-- Table structure for demo_user
-- ----------------------------
DROP TABLE IF EXISTS `demo_user`;
CREATE TABLE `demo_user`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci NULL DEFAULT NULL,
  `delete_flag` int NOT NULL DEFAULT 0,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_croatian_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of demo_user
-- ----------------------------
INSERT INTO `demo_user` VALUES (1, 'admin', 'admin', 0, 'fc4ce319-7fee-4708-ad06-a4b897cc70fa');
INSERT INTO `demo_user` VALUES (2, 'xueyj', 'xueyj', 0, '542bc834-1720-4a12-a954-12cc7012efb8');
INSERT INTO `demo_user` VALUES (3, 'mloine', 'mloine', 0, '974b55cc-e7ce-4546-9c5c-9172bb36cfd1');
INSERT INTO `demo_user` VALUES (4, 'tt', 'tt', 0, 'c3f19546-1eed-4c40-932c-d80a9898dc66');

-- ----------------------------
-- Table structure for demo_user_field
-- ----------------------------
DROP TABLE IF EXISTS `demo_user_field`;
CREATE TABLE `demo_user_field`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci NULL DEFAULT NULL,
  `function_id` bigint NOT NULL,
  `delete_flag` int NOT NULL DEFAULT 0,
  `desct` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci NULL DEFAULT NULL,
  `user_id` bigint NOT NULL,
  `condition_txt` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci NULL DEFAULT NULL,
  `field_id` bigint NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 59 CHARACTER SET = utf8 COLLATE = utf8_croatian_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of demo_user_field
-- ----------------------------
INSERT INTO `demo_user_field` VALUES (27, 'rawPrice', 'java.math.BigDecimal', 1, 0, '原价', 2, NULL, 1);
INSERT INTO `demo_user_field` VALUES (28, 'goodsType', 'java.lang.Integer', 1, 0, '服务类型', 2, NULL, 2);
INSERT INTO `demo_user_field` VALUES (29, 'vendorName', 'java.lang.String', 1, 0, '供应商名称', 2, '#B == \'与光小草\'', 3);
INSERT INTO `demo_user_field` VALUES (42, 'rawPrice', 'java.math.BigDecimal', 1, 0, '原价', 3, '#B < 100', 1);
INSERT INTO `demo_user_field` VALUES (43, 'goodsType', 'java.lang.Integer', 1, 0, '服务类型', 3, NULL, 2);
INSERT INTO `demo_user_field` VALUES (44, 'vendorName', 'java.lang.String', 1, 0, '供应商名称', 3, NULL, 3);
INSERT INTO `demo_user_field` VALUES (45, 'sku', 'java.lang.String', 1, 0, '商品sku', 3, NULL, 4);
INSERT INTO `demo_user_field` VALUES (50, 'rawPrice', 'java.math.BigDecimal', 1, 0, '原价', 4, NULL, 1);
INSERT INTO `demo_user_field` VALUES (51, 'goodsType', 'java.lang.Integer', 1, 0, '服务类型', 4, NULL, 2);
INSERT INTO `demo_user_field` VALUES (52, 'vendorName', 'java.lang.String', 1, 0, '供应商名称', 4, '#B == \'与光小草\' or #B == \'天线宝宝\'', 3);
INSERT INTO `demo_user_field` VALUES (53, 'sku', 'java.lang.String', 1, 0, '商品sku', 4, NULL, 4);
INSERT INTO `demo_user_field` VALUES (54, 'rawPrice', 'java.math.BigDecimal', 1, 0, '原价', 1, NULL, 1);
INSERT INTO `demo_user_field` VALUES (55, 'goodsType', 'java.lang.Integer', 1, 0, '服务类型', 1, NULL, 2);
INSERT INTO `demo_user_field` VALUES (56, 'vendorName', 'java.lang.String', 1, 0, '供应商名称', 1, NULL, 3);
INSERT INTO `demo_user_field` VALUES (57, 'sku', 'java.lang.String', 1, 0, '商品sku', 1, NULL, 4);
INSERT INTO `demo_user_field` VALUES (58, 'isEnable', 'java.lang.Integer', 1, 0, '启用', 1, NULL, 5);

-- ----------------------------
-- Table structure for pd_goods
-- ----------------------------
DROP TABLE IF EXISTS `pd_goods`;
CREATE TABLE `pd_goods`  (
  `id` bigint NOT NULL COMMENT 'id',
  `sku` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品sku',
  `server_time` decimal(10, 2) NULL DEFAULT NULL COMMENT '服务时间(天)',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `good_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品描述',
  `raw_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '原价',
  `discount` decimal(10, 2) NULL DEFAULT NULL COMMENT '优惠价格',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '到手价格',
  `vendor_id` bigint NULL DEFAULT NULL COMMENT '供应商主键',
  `vendor_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商名称',
  `icorn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品小图片',
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片路径',
  `goods_type` int NULL DEFAULT NULL COMMENT '服务类型',
  `is_enable` tinyint NOT NULL DEFAULT 1 COMMENT '是否启用',
  `delete_flag` tinyint NOT NULL DEFAULT 0 COMMENT '是否删除',
  `create_by` bigint NOT NULL DEFAULT 0 COMMENT '创建人ID',
  `create_by_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建人名称',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` bigint NOT NULL DEFAULT 0 COMMENT '最后更新人ID',
  `update_by_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '最后修改人名称',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pd_goods
-- ----------------------------
INSERT INTO `pd_goods` VALUES (1, '00001', 1.00, '00001号商品2', 'sunt ex cupidatat Ut', 92.00, 38.00, 2.00, 1726677521396285442, '与光小草', 'http://www.baidu.com', 'https://panda-naixin.oss-cn-hangzhou.aliyuncs.com/pandaBack/1701187361075.png', 1, 0, 0, 1, 'admin', '2023-11-06 16:42:42', 1, 'admin', '2023-12-20 08:35:43');
INSERT INTO `pd_goods` VALUES (2, 'bbac', 0.10, '陪诊服务', 'sunt ex cupidatat Ut', 92.00, 38.00, 54.00, 4, '天线宝宝', NULL, 'http://dummyimage.com/400x400', 2, 1, 0, 1, 'admin', '2023-11-13 00:28:57', 3, 'test2', '2023-12-20 08:35:43');
INSERT INTO `pd_goods` VALUES (3, 'b111bac', 0.50, '陪诊服务', 'sunt ex cupidatat Ut', 92.00, 38.00, 54.00, 4, '天线宝宝', NULL, 'http://dummyimage.com/400x400', 2, 1, 0, 3, 'test2', '2023-11-13 20:31:05', 1724819013902143490, 'aaron', '2023-12-20 08:35:44');
INSERT INTO `pd_goods` VALUES (4, '1', 6.00, '2', '3', 4.00, -1.00, 5.00, 2, NULL, NULL, 'http://dummyimage.com/400x400', 1, 1, 1, 3, 'test2', '2023-11-13 20:57:38', 3, 'test2', '2023-12-20 08:35:44');
INSERT INTO `pd_goods` VALUES (5, 'ggk-1', 1.00, 'tggktest1', 'tggktest1', 120.00, 110.00, 10.00, 1723374553070731265, NULL, NULL, 'http://dummyimage.com/400x400', 5, 1, 1, 1724819013902143490, 'aaron', '2023-11-17 15:49:02', 1724819013902143490, 'aaron', '2023-12-20 08:35:44');
INSERT INTO `pd_goods` VALUES (6, '陪诊服务', 1.00, '陪诊服务', '陪诊服务', 1.00, 0.00, 1.00, 1723375282258870274, '测试渠道商', NULL, 'http://dummyimage.com/400x400', 6, 1, 0, 1724819013902143490, 'aaron', '2023-11-17 17:58:00', 1724819013902143490, 'aaron', '2023-12-20 08:35:45');
INSERT INTO `pd_goods` VALUES (7, '1231231231', 1.50, '职工体检', '职工体检', 1000.00, -29000.00, 30000.00, 1723375282258870274, '测试渠道商', NULL, 'http://dummyimage.com/400x400', 2, 1, 0, 1723743251065352194, 'demo', '2023-11-20 22:47:44', 3, 'test2', '2023-12-20 08:35:59');
INSERT INTO `pd_goods` VALUES (8, '12345678', 1.00, '入职体检', '入职体检', 1000.00, -999.00, 1.00, 1723376039553040386, '第二渠道商', NULL, 'http://dummyimage.com/400x400', 2, 1, 0, 1723743251065352194, 'demo', '2023-11-20 23:58:59', 1723743251065352194, 'demo', '2023-12-20 08:35:58');
INSERT INTO `pd_goods` VALUES (9, '90347526749234', 2.00, '孕检', '孕检', 1000.00, 999.00, 1.00, 1723375282258870274, '测试渠道商', NULL, 'http://dummyimage.com/400x400', 2, 0, 0, 1723743251065352194, 'demo', '2023-11-21 01:16:42', 1724819013902143490, 'aaron', '2023-12-20 08:35:47');
INSERT INTO `pd_goods` VALUES (10, '00001', 13.00, '荣耀手机V10', '荣耀手机V10非常的便宜好用', 299900.00, 111100.00, 188800.00, 1726677521396285442, '与光小草', NULL, 'http://dummyimage.com/400x400', 1, 0, 0, 1724819013902143490, 'aaron', '2023-11-21 22:04:18', 1, 'admin', '2023-12-20 08:35:48');
INSERT INTO `pd_goods` VALUES (11, '11', 1.40, '体检套餐', '33', 200.00, 100.00, 100.00, 2, '祥龙弟弟', NULL, 'https://panda-naixin.oss-cn-hangzhou.aliyuncs.com/pandaBack/1701189091815.png', 1, 1, 0, 3, 'test2', '2023-11-23 22:54:13', 1723743251065352194, 'demo', '2023-12-20 08:35:49');
INSERT INTO `pd_goods` VALUES (12, '1', 6.10, '2', '3', 500.00, 100.00, 400.00, 1723375282258870274, '测试渠道商', NULL, 'https://panda-naixin.oss-cn-hangzhou.aliyuncs.com/undefined', 6, 0, 0, 3, 'test2', '2023-11-23 23:09:08', 1724819013902143490, 'aaron', '2023-12-20 08:35:50');
INSERT INTO `pd_goods` VALUES (13, '1213123', 6.00, '2', '3', 500.00, 100.00, 400.00, 1727670683266867202, '嘎嘎嘎', NULL, 'https://panda-naixin.oss-cn-hangzhou.aliyuncs.com/pandaBack/1700752207075.png', 3, 1, 0, 3, 'test2', '2023-11-23 23:10:46', 3, 'test2', '2023-12-20 08:35:51');
INSERT INTO `pd_goods` VALUES (14, 'rujjkkk-001', 1.00, '锐捷（Ruijie）星耀天蝎电竞路由器X60PRO ', '锐捷（Ruijie）星耀天蝎电竞路由器X60PRO 无线千兆WiFi6 穿墙王ax6000 5G游戏加速 6000M 2.5G网口', 199900.00, 130000.00, 69900.00, 1728483129216917506, '相亲相爱供应商', NULL, 'https://panda-naixin.oss-cn-hangzhou.aliyuncs.com/pandaBack/1700981036781.png', 1, 1, 0, 1724819013902143490, 'aaron', '2023-11-26 02:53:46', 1724819013902143490, 'aaron', '2023-12-20 08:35:52');
INSERT INTO `pd_goods` VALUES (15, 'test2077', 0.50, 'test2077', 'test2077', 12000.00, 2000.00, 10000.00, 2, '祥龙弟弟', NULL, NULL, 1, 1, 0, 1, 'admin', '2023-12-06 13:59:35', 1, 'admin', '2023-12-20 08:35:53');

SET FOREIGN_KEY_CHECKS = 1;
